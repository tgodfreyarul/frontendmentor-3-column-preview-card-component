import React from 'react';
import './App.scss';
import CardsContainer from './components/CardsContainer/CardsContainer';
import sedanLogo from './assets/images/icon-sedans.svg';
import suvLogo from './assets/images/icon-suvs.svg';
import luxuryLogo from './assets/images/icon-luxury.svg';

// Data for the cards, with imported images used for logos
const cardData = [
  {
    id: 1,
    logo: sedanLogo, // Use the imported image
    title: 'SEDANS',
    description: 'Choose a sedan for its affordability and excellent fuel economy. Ideal for cruising in the city or on your next road trip.',
    buttonText: 'Learn More',
    bgColor: 'hsl(31, 77%, 52%)',
    btnTextColor: 'hsl(31, 77%, 52%)'
  },
  {
    id: 2,
    logo: suvLogo, // Use the imported image
    title: 'SUVS',
    description: 'Take an SUV for its spacious interior, power, and versatility. Perfect for your next family vacation and off-road adventures.',
    buttonText: 'Learn More',
    bgColor: 'hsl(184, 100%, 22%)',
    btnTextColor: 'hsl(184, 100%, 22%)'
  },
  {
    id: 3,
    logo: luxuryLogo, // Use the imported image
    title: 'LUXURY',
    description: 'Cruise in the best car brands without the bloated prices. Enjoy the enhanced comfort of a luxury rental and arrive in style.',
    buttonText: 'Learn More',
    bgColor: 'hsl(179, 100%, 13%)',
    btnTextColor: 'hsl(179, 100%, 13%)'
  }
];

function App() {
  return (
    <div className="app">
      <CardsContainer cards={cardData} />
    </div>
  );
}

export default App;
