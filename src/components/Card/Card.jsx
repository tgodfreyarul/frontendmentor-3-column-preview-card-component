// Card.jsx
import React from 'react';
import './Card.scss';

const Card = ({ logo, title, description, buttonText, bgColor, btnTextColor, type }) => {
  const cardClass = `card ${type}`;
  const cardStyle = {
    backgroundColor: bgColor,
  };

  return (
    <div className={cardClass} style={cardStyle}>
      <img src={logo} alt={`${title} logo`} className="card-logo" />
      <h2>{title}</h2>
      <p>{description}</p>
      <button className="card-button">
        <span>{buttonText}</span>
      </button>
    </div>
  );
};

export default Card;
