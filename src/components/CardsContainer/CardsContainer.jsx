// src/components/CardsContainer/CardsContainer.jsx
import React from 'react';
import Card from '../Card/Card';
import './CardsContainer.scss';

const CardsContainer = ({ cards }) => {
  return (
    <div className="cards-container">
      {cards.map(card => (
        <Card
          key={card.id}
          logo={card.logo}
          title={card.title}
          description={card.description}
          buttonText={card.buttonText}
          bgColor={card.bgColor}
          btnTextColor={card.btnTextColor}
          type={card.title.toLowerCase()}
        />
      ))}
    </div>
  );
};

export default CardsContainer;
